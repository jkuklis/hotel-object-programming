package hotel;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Questionnaire {
	private GregorianCalendar arrival;
	private int length_of_stay;
	private int occupancy;
	private int price;
	private Style style;
	private Colour colour;
	private Outlook outlook;
	private boolean internet_connection;

	public Questionnaire(GregorianCalendar arrival, int length_of_stay,
			int occupancy, int price, Style style, Colour colour,
			Outlook outlook, boolean internet_connection) {
		this.arrival = arrival;
		this.length_of_stay = length_of_stay;
		this.occupancy = occupancy;
		this.price = price;
		this.style = style;
		this.colour = colour;
		this.outlook = outlook;
		this.internet_connection = internet_connection;
	}

	public Period make_period() {
		return new Period(arrival, length_of_stay);
	}

	public static String format(GregorianCalendar calendar) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		fmt.setCalendar(calendar);
		String dateFormatted = fmt.format(calendar.getTime());
		return dateFormatted;
	}

	public GregorianCalendar get_arrival() {
		return arrival;
	}

	public int get_length_of_stay() {
		return length_of_stay;
	}

	public int get_occupancy() {
		return occupancy;
	}

	public int get_price() {
		return price;
	}

	public Style get_style() {
		return style;
	}

	public Colour get_colour() {
		return colour;
	}

	public Outlook get_outlook() {
		return outlook;
	}

	public boolean get_internet_connection() {
		return internet_connection;
	}

	@Override
	public String toString() {
		return (format(arrival) + ", " + length_of_stay + " days, " + occupancy
				+ " people, price: " + price + ", " + style + ", " + colour
				+ ", " + outlook + ", " + (internet_connection == true ? "yes"
					: "no"));
	}
}