package hotel;

public enum Colour {
	GRAY, STEEL_GRAY, PURPLE, SEA_BLUE, WILLOW_GREEN, LIGHT_GREEN;
	@Override
	public String toString() {
		switch (this) {
		case GRAY:
			return "gray";
		case STEEL_GRAY:
			return "steel gray";
		case PURPLE:
			return "purple";
		case SEA_BLUE:
			return "sea blue";
		case WILLOW_GREEN:
			return "willow green";
		case LIGHT_GREEN:
			return "light green";
		default:
			return "COLOUR_ERROR";
		}
	}
}
