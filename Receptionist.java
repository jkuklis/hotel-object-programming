package hotel;

public class Receptionist extends Person {

	public Receptionist(String first_name, String surname, Receptionist_Strategy strategy) {
		super(first_name, surname, strategy);
	}

	public Room examine(Order order, Room[] rooms) {
		Room room_suggestion = get_real_strategy().choose(rooms,
				order.get_questionnaire());
		return room_suggestion;
	}

	Receptionist_Strategy get_real_strategy() {
		return (Receptionist_Strategy) get_strategy();
	}

	boolean same(Receptionist receptionist) {
		return (get_id() == receptionist.get_id());
	}

	@Override
	protected String occupation() {
		return "Receptionist";
	}
}