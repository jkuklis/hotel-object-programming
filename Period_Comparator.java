package hotel;

import java.util.Comparator;

public class Period_Comparator implements Comparator<Period> {

	@Override
	public int compare (Period p1, Period p2) {
		return (p1.get_arrival().compareTo(p2.get_arrival()));
	}

}
