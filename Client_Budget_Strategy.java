package hotel;

public class Client_Budget_Strategy extends Client_Strategy {

	Client_Budget_Strategy() {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return (room.get_price() <= questionnaire.get_price());
	}

	@Override
	protected String type() {
		return "budget";
	}

}
