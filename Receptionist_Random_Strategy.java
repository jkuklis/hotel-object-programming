package hotel;

import java.util.ArrayList;
import java.util.Random;

public class Receptionist_Random_Strategy extends Receptionist_Strategy {

	Receptionist_Random_Strategy () {}
	
	@Override
	protected Room choose(Room[] rooms, Questionnaire questionnaire) {
		ArrayList<Room> possible = new ArrayList<Room>();
		for (Room room : rooms) {
			if (room.check_availability(questionnaire)) {
				possible.add(room);
			}
		}
		if (possible.size() == 0) {
			return null;
		} else {
			Random generator = new Random();
			int which_room = generator.nextInt(possible.size());
			return rooms[which_room];
		}
	};

	@Override
	protected Room pick(Room first, Room second, Questionnaire questionnaire) {
		return first;
	}

	@Override
	protected String type() {
		return "random";
	}
}