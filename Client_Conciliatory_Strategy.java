package hotel;

public class Client_Conciliatory_Strategy extends Client_Strategy {
	
	Client_Conciliatory_Strategy() {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return true;
	}

	@Override
	protected String type() {
		return "conciliatory";
	}
}
