package hotel;

public enum Outlook {
	NORTH, EAST, SOUTH, WEST;
	@Override
	public String toString() {
		switch (this) {
		case NORTH:
			return "north";
		case EAST:
			return "east";
		case SOUTH:
			return "south";
		case WEST:
			return "west";
		default:
			return "OUTLOOK_ERROR";
		}
	}
}
