package hotel;

public class Client_Outlook_Strategy extends Client_Strategy {

	Client_Outlook_Strategy() {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return (room.get_outlook() == Outlook.NORTH);
	}

	@Override
	protected String type() {
		return "outlook";
	}

}
