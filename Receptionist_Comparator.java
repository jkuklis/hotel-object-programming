package hotel;

import java.util.Comparator;

public class Receptionist_Comparator implements Comparator<Receptionist> {

	@Override
	public int compare (Receptionist r1, Receptionist r2) {
		if (r1.get_id() > r2.get_id()) return 1;
		else if (r1.get_id() < r2.get_id()) return -1;
		else return 0;
	}
}
