package hotel;

public abstract class Receptionist_Strategy extends Strategy {

	Receptionist_Strategy () {}
		
	protected Room choose(Room[] rooms, Questionnaire questionnaire) {
		Room chosen = null;
		for (int i = 0; i < rooms.length; i++) {
			Room room = rooms[i];
			if (approves(room, questionnaire)) {
				if (chosen == null) {
					chosen = room;
				} else {
					chosen = pick(room, chosen, questionnaire);
				}
			}
		}
		return chosen;
	};

	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return room.check_availability(questionnaire);
	}
	
	protected abstract Room pick(Room first, Room second,
			Questionnaire questionnaire);
}