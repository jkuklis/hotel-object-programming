package hotel;

import java.util.Comparator;

public class Room_Comparator implements Comparator<Room> {

	@Override
	public int compare (Room r1, Room r2) {
		if (r1.get_id() > r2.get_id()) return 1;
		else if (r1.get_id() < r2.get_id()) return -1;
		else return 0;
	}

}
