package hotel;

public class Order {
	private Client client;
	private Questionnaire questionnaire;
	private int times_examined;

	public Order(Client client, Questionnaire questionnaire) {
		this.client = client;
		this.questionnaire = questionnaire;
		this.times_examined = 0;
	}

	public boolean still_active() {
		return (times_examined < 3 ? true : false);
	}

	public void update() {
		times_examined++;
	}

	public Period get_period() {
		return questionnaire.make_period();
	}

	public boolean decide (Room room) {
		return client.decide(room, questionnaire);
	}
	
	public Client get_client() {
		return client;
	}

	public Questionnaire get_questionnaire() {
		return questionnaire;
	}

	@Override
	public String toString() {
		return ("Order: " + questionnaire);
	}
}