package hotel;

public class Receptionist_Approximate_Strategy extends Receptionist_Strategy {

	Receptionist_Approximate_Strategy () {}

	@Override
	protected Room pick(Room first, Room second, Questionnaire questionnaire) {
		int first_fulfilled = first.fulfilled_requirements(questionnaire);
		int second_fulfilled = second.fulfilled_requirements(questionnaire);
		if (first_fulfilled > second_fulfilled)
			return first;
		else if (second_fulfilled > first_fulfilled)
			return second;
		else {
			if (first.get_price() > second.get_price())
				return first;
			else if (second.get_price() > first.get_price())
				return second;
			else {
				if (first.get_id() < second.get_id())
					return first;
				else
					return second;
			}
		}
	}

	@Override
	protected String type() {
		return "approximate";
	}
}