package hotel;

public abstract class Strategy {
	
	Strategy () {}
	
	protected abstract String type();

	protected abstract boolean approves(Room room, Questionnaire questionnaire);

	@Override
	public String toString() {
		return type();
	}
}