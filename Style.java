package hotel;

public enum Style {
	ORIENTAL, NAUTICAL, MODERN, RUSTIC, ART_NOUVEAU;
	@Override
	public String toString() {
		switch (this) {
		case ORIENTAL:
			return "oriental";
		case NAUTICAL:
			return "nautical";
		case MODERN:
			return "modern";
		case RUSTIC:
			return "rustic";
		case ART_NOUVEAU:
			return "art nouveau";
		default:
			return "STYLE_ERROR";
		}
	}
}
