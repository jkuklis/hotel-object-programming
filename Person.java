package hotel;

public abstract class Person {
	
	private static int how_many = 0;
	private String first_name;
	private String surname;
	private Strategy strategy;
	private int id;
	
	Person (String first_name, String surname, Strategy strategy) {
		this.first_name = first_name;
		this.surname = surname;
		this.strategy = strategy;
		this.id = how_many;
		how_many++;
	}
	
	protected abstract String occupation();
	
	protected Strategy get_strategy() {
		return strategy;
	};
	
	public int get_id() {
		return id;
	}
	
	@Override
	public String toString() {
		return (occupation() + ": " + first_name + " " + surname + ", " + strategy);
	}
}