package hotel;

import java.util.TreeSet;

public class Room {
	private int id;
	private int max_occupancy;
	private int price;
	private Style style;
	private Colour colour;
	private Outlook outlook;
	private boolean internet_connection;
 	private TreeSet<Period> when_occupied;
	
	public Room(int id, int max_occupancy, int price, Style style,
			Colour colour, Outlook outlook, boolean internet_connection) {
		this.id = id;
		this.max_occupancy = max_occupancy;
		this.price = price;
		this.style = style;
		this.colour = colour;
		this.outlook = outlook;
		this.internet_connection = internet_connection;
		this.when_occupied = new TreeSet<Period>(new Period_Comparator());
	}

	public boolean check_availability(Questionnaire questionnaire) {
		boolean available = true;
		Period requested = questionnaire.make_period();
		Period floor = when_occupied.floor(requested);
		Period ceiling = when_occupied.ceiling(requested);
		
		if (floor != null && requested.collide(floor))
			available = false;
		if (ceiling != null && requested.collide(ceiling))
			available = false;
		return available;
	}

	public void reserve(Order order) {
		when_occupied.add(order.get_period());
	}

	protected int fulfilled_requirements(Questionnaire questionnaire) {
		int fulfilled = 0;
		if (max_occupancy >= questionnaire.get_occupancy())
			fulfilled++;
		if (price <= questionnaire.get_price())
			fulfilled++;
		if (style == questionnaire.get_style())
			fulfilled++;
		if (colour == questionnaire.get_colour())
			fulfilled++;
		if (outlook == questionnaire.get_outlook())
			fulfilled++;
		if (internet_connection == questionnaire.get_internet_connection())
			fulfilled++;
		return fulfilled;
	}

	boolean same(Room room) {
		return (id == room.get_id());
	}

	public int get_id() {
		return id;
	}

	public int get_price() {
		return price;
	}

	public Outlook get_outlook() {
		return outlook;
	}

	@Override
	public String toString() {
		return ("room: max " + max_occupancy + " people, price: " + price + ", " + style + ", "
				+ colour + ", " + outlook + ", " + (internet_connection == true ? "yes"
					: "no"));
	}
}