package hotel;

import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeSet;

public class Hotel {
	
	private TreeSet<Room> rooms;
	private TreeSet<Receptionist> receptionists;

	public Hotel(Room[] rooms, Receptionist[] receptionists) {
		// if rooms == null, then hotel has no rooms
		if (rooms != null) {
			this.rooms = new TreeSet<Room>(new Room_Comparator());
			for (int i = 0; i < rooms.length; i++) {
				if (rooms[i] == null) {
					System.out.println("NULL_ROOM_ERROR");
				} else {
					this.rooms.add(rooms[i]);
				}
			}
		}
		// if receptionists == null, then hotel has no receptionists
		if (receptionists != null) {
			this.receptionists = new TreeSet<Receptionist>(
					new Receptionist_Comparator());
			for (int i = 0; i < receptionists.length; i++) {
				if (receptionists[i] == null) {
					System.out.println("NULL_RECEPTIONIST_ERROR");
				} else {
					this.receptionists.add(receptionists[i]);
				}
			}
		}
	}

	public void accept(Order[] orders, Room[] rooms,
			Receptionist[] receptionists) {
		if (!check_orders(orders) || !check_receptionists(receptionists)
				|| !check_rooms(rooms)) {
			System.out.println("ACCEPT_ERROR");
		} else {
			Queue<Order> queue = new LinkedList<Order>();
			int which_receptionist = 0;
			for (int i = 0; i < orders.length; i++) {
				queue.add(orders[i]);
			}
			while (!queue.isEmpty()) {
				boolean decision;
				Order order = queue.remove();
				Room room_suggestion = receptionists[which_receptionist]
						.examine(order, rooms);
				if (room_suggestion == null)
					decision = false;
				else
					decision = order.decide(room_suggestion);
				order.update();
				if (decision) {
					room_suggestion.reserve(order);
				} else if (order.still_active()) {
					queue.add(order);
				} else {
				}
				System.out.println(receptionists[which_receptionist]);
				which_receptionist++;
				which_receptionist %= receptionists.length;
				System.out.println(order);
				System.out.println(room_suggestion == null ? "No suggestion"
						: "Suggested " + room_suggestion);
				System.out.println(order.get_client());
				System.out.println(decision == true ? "yes" : "no");
			}
		}
	}

	public Order get_Order(Client client, Questionnaire questionnaire) {
		return new Order(client, questionnaire);
	}

	private boolean check_orders(Order[] orders) {
		if (orders == null)
			return false;
		for (int i = 0; i < orders.length; i++) {
			if (orders[i] == null)
				return false;
		}
		return true;
	}

	private boolean check_receptionists(Receptionist[] receptionists) {
		if (receptionists == null || this.receptionists == null)
			return false;
		for (int i = 0; i < receptionists.length; i++) {
			if (receptionists[i] == null)
				return false;
			Receptionist possible = this.receptionists.floor(receptionists[i]);
			if (possible == null
					|| possible.get_id() < receptionists[i].get_id())
				return false;
		}
		return true;
	}

	private boolean check_rooms(Room[] rooms) {
		if (rooms == null || this.rooms == null)
			return false;
		for (int i = 0; i < rooms.length; i++) {
			if (rooms[i] == null)
				return false;
			Room possible = this.rooms.floor(rooms[i]);
			if (possible == null || possible.get_id() < rooms[i].get_id())
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		Client_Strategy cbs = new Client_Budget_Strategy();
		Client_Strategy ccs = new Client_Conciliatory_Strategy();
		Client_Strategy chs = new Client_HalfWay_Strategy();
		Client_Strategy cos = new Client_Outlook_Strategy();
		Client_Strategy cps = new Client_Perfect_Strategy();
		Client cb = new Client("Anthony", "Hopkins", cbs);
		Client cc = new Client("John", "Travolta", ccs);
		Client ch = new Client("Forrest", "Gump", chs);
		Client co = new Client("Marlon", "Brando", cos);
		Client cp = new Client("Artur", "Andrus", cps);
		Receptionist_Strategy ras = new Receptionist_Approximate_Strategy();
		Receptionist_Strategy rns = new Receptionist_Nasty_Strategy();
		Receptionist_Strategy rps = new Receptionist_Perfect_Strategy();
		Receptionist_Strategy rrs = new Receptionist_Random_Strategy();
		Receptionist ra = new Receptionist("John", "Snow", ras);
		Receptionist rn = new Receptionist("Ron", "Paul", rns);
		Receptionist rp = new Receptionist("Adam", "Smith", rps);
		Receptionist rr = new Receptionist("Max", "Payne", rrs);
		Receptionist not_working = new Receptionist("Jeremy", "Clarkson", rns);
		Receptionist[] hotel_receptionists = { ra, rn, rp, rr };
		Receptionist[] error_receptionists = { not_working };
		Room room1 = new Room(1, 3, 40, Style.ART_NOUVEAU, Colour.GRAY,
				Outlook.EAST, true);
		Room room2 = new Room(2, 4, 20, Style.ART_NOUVEAU, Colour.GRAY,
				Outlook.EAST, true);
		Room room3 = new Room(3, 2, 50, Style.MODERN, Colour.STEEL_GRAY,
				Outlook.NORTH, true);
		Room room4 = new Room(4, 3, 25, Style.NAUTICAL, Colour.LIGHT_GREEN,
				Outlook.EAST, true);
		Room room5 = new Room(5, 6, 10, Style.ORIENTAL, Colour.WILLOW_GREEN,
				Outlook.SOUTH, false);
		Room room6 = new Room(6, 3, 30, Style.MODERN, Colour.GRAY,
				Outlook.WEST, true);
		Room[] hotel_rooms = { room1, room2, room3, room4, room5 };
		Room[] error_rooms = { room6 };
		Hotel hotel = new Hotel(hotel_rooms, hotel_receptionists);
		Questionnaire q1 = new Questionnaire(
				new GregorianCalendar(1996, 11, 19), 3, 3, 40, Style.MODERN,
				Colour.PURPLE, Outlook.WEST, true);
		Questionnaire q2 = new Questionnaire(
				new GregorianCalendar(1996, 11, 20), 3, 3, 40, Style.MODERN,
				Colour.PURPLE, Outlook.WEST, true);
		Questionnaire q3 = new Questionnaire(
				new GregorianCalendar(1996, 11, 25), 5, 5, 5,
				Style.ART_NOUVEAU, Colour.GRAY, Outlook.EAST, true);
		Questionnaire q4 = new Questionnaire(
				new GregorianCalendar(1996, 12, 1), 4, 1, 22,
				Style.ART_NOUVEAU, Colour.GRAY, Outlook.NORTH, false);
		Questionnaire q5 = new Questionnaire(
				new GregorianCalendar(1996, 11, 25), 1, 1, 10, Style.ORIENTAL,
				Colour.GRAY, Outlook.EAST, true);
		Order o1 = hotel.get_Order(cb, q1);
		Order o2 = hotel.get_Order(cc, q2);
		Order o3 = hotel.get_Order(ch, q3);
		Order o4 = hotel.get_Order(co, q4);
		Order o5 = hotel.get_Order(cp, q5);
		Order[] orders = { o1, o2, o3, o4, o5 };
		hotel.accept(orders, hotel_rooms, hotel_receptionists);
		/*
		// only room reserved:
		Order[] same_orders = { hotel.get_Order(cb, q1),
				hotel.get_Order(cc, q1) };
		Room[] only_room = { room1 };
		hotel.accept(same_orders, only_room, hotel_receptionists);
		*/
		// Error tests:
		// hotel.accept(null, hotel_rooms, hotel_receptionists);
		// hotel.accept(orders, null, hotel_receptionists);
		// hotel.accept(orders, hotel_rooms, null);
		// hotel.accept(orders, error_rooms, hotel_receptionists);
		// hotel.accept(orders, hotel_rooms, error_receptionists);
	}
}