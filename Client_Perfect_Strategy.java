package hotel;

public class Client_Perfect_Strategy extends Client_Strategy {

	Client_Perfect_Strategy() {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return (room.fulfilled_requirements(questionnaire) == 6);
	}

	@Override
	protected String type() {
		return "perfect";
	}
}
