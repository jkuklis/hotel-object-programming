package hotel;

public class Client extends Person {
	
	Client (String first_name, String surname, Client_Strategy strategy) {
		super(first_name, surname, strategy);
	}
	
	public boolean decide (Room room, Questionnaire questionnaire) {
		return get_real_strategy().approves(room, questionnaire);
	}
	
	Client_Strategy get_real_strategy() {
		return (Client_Strategy)get_strategy();
	}
	
	@Override
	protected String occupation () {
		return "Client";
	}
}