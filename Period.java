package hotel;

import java.util.GregorianCalendar;

public class Period {

	private GregorianCalendar arrival;
	private int length;
	private GregorianCalendar departure;

	public Period(GregorianCalendar arrival, int length) {
		this.arrival = arrival;
		this.length = length;
		this.departure = arrival;
		this.departure.add(3,length);
	}
	
	public boolean collide(Period period) {
		boolean check;
		if (arrival.compareTo(period.get_arrival()) <= 0) {
			if (departure.compareTo(period.get_departure()) >= 0) {
				check = true;
			} else {
				check = false;
			}
		}
		else {
			if (arrival.compareTo(period.get_departure()) <= 0) {
				check = true;
			} else {
				check = false;
			}
		}
		return check;
	}

	protected GregorianCalendar get_arrival() {
		return arrival;
	}
	
	protected GregorianCalendar get_departure() {
		return departure;
	}
	
	@Override
	public String toString() {
		return (arrival + ", " + length + ", " + departure);
	}
}