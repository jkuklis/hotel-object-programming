package hotel;

public class Client_HalfWay_Strategy extends Client_Strategy {

	Client_HalfWay_Strategy() {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		return (room.fulfilled_requirements(questionnaire) >= 3);
	}

	@Override
	protected String type() {
		return "half-way";
	}

}
