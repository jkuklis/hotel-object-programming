package hotel;

public class Receptionist_Perfect_Strategy extends Receptionist_Strategy {
	
	Receptionist_Perfect_Strategy () {}
	
	@Override
	protected boolean approves(Room room, Questionnaire questionnaire) {
		boolean answer = true;
		if (!room.check_availability(questionnaire)
				|| room.fulfilled_requirements(questionnaire) < 6) {
			answer = false;
		}
		return answer;
	}

	@Override
	protected Room pick(Room first, Room second, Questionnaire questionnaire) {
		if (first.get_id() < second.get_id())
			return first;
		else
			return second;
	}

	@Override
	protected String type() {
		return "perfect";
	}
}